package dao;

import dao.modelo.Informe1;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperCompileManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class DaoInformes {

    private static final String query_informe_1 = "select sum(e.precio) as Gasto from Espacio e inner join Reservas r on e.id = r.id_espacio where r.id_user=?";

    public void generarInforme1(int id) {



        try {
            //Conexión a la BBDD (postgresql)
            Connection conn = DriverManager.getConnection("jdbc:mysql://@dam2.mysql.iesquevedo.es:3335", "root", "quevedo2020");

            //Cargar el informe
            String report = "./gastosTotales.jrxml";
            JasperReport jasperReport = JasperCompileManager.compileReport(report);
            //Rellenar el informe

            JasperPrint jp = JasperFillManager.fillReport(jasperReport, null, conn);

            //Exportar a pdf
            String pdf = "./gastosTotales.pdf";
            JasperExportManager.exportReportToPdfFile(jp, pdf);

            //Abrir el pdf directamente desde java
            JasperViewer.viewReport(jp, false);

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            System.exit(0);
        }
    }

    public void generarInforme2() {
    }

    public void generarInforme3() {
    }
}
