package dao;

import dao.modelo.Espacio;
import dao.modelo.Reserva;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DaoReservas {
    private String ADD_RESERVA = "insert into Reservas (id_user,id_espacio,activa,fecha) " +
            "values (?,?,?,?)";

    private String SELECT_RESERVAS = "select * from Reservas r inner join Espacio e on r.id_espacio=e.id " +
            "where r.id_user = ? and r.activa is true ";

    private static String ACTUALIZAR_RESERVA =
            "update Reservas r set r.fecha = ? " +
                    "where r.id = ?";

    private static String CANCELAR_RESERVA =
            "delete from Reservas r where r.id = ?";

    public Either<String, Boolean> hacerReserva(Reserva reserva) {
        DBConnection db = new DBConnection();
        Connection con = null;
        PreparedStatement stmt = null;

        Either<String, Boolean> result = null;
        try {
            con = db.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement
                    (ADD_RESERVA,
                            Statement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, reserva.getId_User());
            stmt.setInt(2, reserva.getEspacio().getId());
            stmt.setBoolean(3, true);
            stmt.setDate(4, Date.valueOf(reserva.getFecha()));

            stmt.executeUpdate();
            result = Either.right(true);

        } catch (Exception e) {
            log.error(e.getMessage());
            result = Either.left(e.getMessage());
            db.rollbackCon(con);
        } finally {
            db.cerrarStatement(stmt);
            db.cerrarConexion(con);
        }
        return result;

    }

    public Either<String, List<Reserva>> getTodasReservasUsuario(Usuario u) {
        Either<String, List<Reserva>> result = null;
        DBConnection db = new DBConnection();
        Connection connection = null;
        PreparedStatement pst = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        List<Reserva> reservas = new ArrayList<>();
        try {
            connection = db.getConnection();
            stmt = connection.createStatement();
            pst = connection.prepareStatement(SELECT_RESERVAS);
            pst.setInt(1, u.getId());
            resultSet = pst.executeQuery();
            while (resultSet.next()) {
                Reserva r = new Reserva(
                        resultSet.getInt("r.id"),
                        resultSet.getInt("r.id_user"),
                        new Espacio(
                                resultSet.getInt("e.id"),
                                resultSet.getString("e.nombre"),
                                resultSet.getDouble("e.precio"),
                                resultSet.getTimestamp("e.hora_inicio"),
                                resultSet.getTimestamp("e.hora_fin")),
                        resultSet.getDate("r.fecha").toLocalDate(),
                        resultSet.getBoolean("r.activa"));
                reservas.add(r);
            }
            result = Either.right(reservas);
        } catch (Exception e) {
            log.error(e);
            result = Either.left(e.getMessage());
        } finally {
            db.cerrarResultSet(resultSet);
            db.cerrarStatement(stmt);
            db.cerrarConexion(connection);
        }
        return result;
    }

    public Either<String, Boolean> actualizarReserva(Reserva reserva) {
        Either<String, Boolean> result = null;
        DBConnection db = new DBConnection();
        Connection connection = null;
        PreparedStatement stmt = null;
        int resultSetInt = 0;
        try {

            connection = db.getConnection();
            stmt = connection.prepareStatement(ACTUALIZAR_RESERVA);
            stmt.setDate(1, Date.valueOf(reserva.getFecha()));
            stmt.setInt(2, reserva.getId());
            resultSetInt = stmt.executeUpdate();

            if (resultSetInt > 0) {
                result = Either.right(true);

            }
        } catch (Exception e) {
            log.error(e);
            result = Either.left(e.getMessage());
        } finally {
            db.cerrarStatement(stmt);
            db.cerrarConexion(connection);
        }
        return result;
    }

    public Either<String, Boolean> cancelarReserva(Reserva reservaModificar) {
        Either<String, Boolean> result = null;
        DBConnection db = new DBConnection();
        Connection connection = null;
        PreparedStatement stmt = null;
        int resultSetInt = 0;
        try {

            connection = db.getConnection();
            stmt = connection.prepareStatement(CANCELAR_RESERVA);
            stmt.setInt(1, reservaModificar.getId());
            resultSetInt = stmt.executeUpdate();

            if (resultSetInt > 0) {
                result = Either.right(true);
            }
        } catch (Exception e) {
            log.error(e);
            result = Either.left(e.getMessage());
        } finally {
            db.cerrarStatement(stmt);
            db.cerrarConexion(connection);
        }
        return result;
    }
}
