package dao.modelo;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Informe1 {
    double gasto;
}
