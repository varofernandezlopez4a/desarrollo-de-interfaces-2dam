package gui.controllers;

import dao.modelo.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import servicios.ServiciosUsuarios;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class LoginController implements Initializable {


    private String codigo = "pandemials";

    private boolean valid = false;

    @FXML
    private AnchorPane newPane;
    @FXML
    private CheckBox checkCodigo;
    @FXML
    private CheckBox checkNew;
    @FXML
    private TextField textUser;
    @FXML
    private PasswordField textPass;
    @FXML
    private TextField newNameText;
    @FXML
    private TextField newMailText;
    @FXML
    private PasswordField newPassText;

    private Alert alert;
    private Alert alert2;
    ServiciosUsuarios serviciosUsuarios;

    private PrincipalController borderPane;


    public void setBorderPane(PrincipalController borderPane) {
        this.borderPane = borderPane;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.ERROR);
        alert2 = new Alert(Alert.AlertType.INFORMATION);
        serviciosUsuarios = new ServiciosUsuarios();
    }

    public void login(ActionEvent actionEvent) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String usuario = textUser.getText();
        String pass = textPass.getText();
        if (!usuario.isEmpty() && !pass.isEmpty()) {
            serviciosUsuarios.login(usuario, pass).peek(s -> {
                borderPane.setUsuario(s);
                if(borderPane.getUsuario().getTipoUsuario() != 0){
                    borderPane.cargarBienvenida();
                }else{
                    alert2.setContentText("Un administrador no puede entrar a esta parte del programa");
                    alert2.show();
                }
                textUser.setText("");
                textPass.setText("");
            }).peekLeft(message -> {
                alert.setContentText(message);
                alert.showAndWait();
            });
        } else {
            alert.setContentText("Por favor rellene todos los campos");
            alert.showAndWait();
        }
    }

    public void nuevaCuenta(MouseEvent mouseEvent) {
        newPane.setVisible(true);
        checkNew.setSelected(false);
    }

    public void cerrar(ActionEvent actionEvent) {
        newPane.setVisible(false);
        checkCodigo.setSelected(false);
    }

    public void registrar(ActionEvent actionEvent) throws InvalidKeySpecException, NoSuchAlgorithmException {
        Usuario usuario;
        if (valid) {
            usuario = new Usuario(0, newNameText.getText(), newPassText.getText(), newMailText.getText(), 0);
        } else {
            usuario = new Usuario(0, newNameText.getText(), newPassText.getText(), newMailText.getText(), 1);
        }
        if (validarUsuario(usuario).isEmpty()) {
            serviciosUsuarios.registrarse(usuario).peek(s -> {
                borderPane.setUsuario(usuario);
                if(borderPane.getUsuario().getTipoUsuario() != 0){
                    borderPane.cargarBienvenida();
                }else{
                    alert2.setContentText("Registrado");
                    alert2.show();
                    newNameText.setText("");
                    newPassText.setText("");
                    newMailText.setText("");
                }
            }).peekLeft(message -> {
                alert.setContentText(message);
                alert.showAndWait();
            });
        } else {
            alert.setContentText(validarUsuario(usuario));
            alert.showAndWait();
        }

    }

    public void probarCodigo(MouseEvent mouseEvent) {
        checkCodigo.setSelected(false);
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Verificacion");
        dialog.setHeaderText("Introduce el codigo necesario para poder ser administrador");


        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (result.get().equals(codigo)) {
                alert2.setContentText("Codigo valido");
                alert2.showAndWait();
                valid = true;
                checkCodigo.setSelected(true);
            } else {
                alert.setContentText("Codigo incorrecto");
                alert.showAndWait();
                valid = false;
            }
        }
    }

    public String validarUsuario(Usuario object) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();

        return validator.validate(object)
                .stream()
                .map(filtroConstraintViolation -> filtroConstraintViolation.getMessage())
                .collect(Collectors.joining(", "));

    }
}
