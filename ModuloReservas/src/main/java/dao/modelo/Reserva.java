package dao.modelo;


import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Reserva {
    private int id;
    private int id_User;
    private Espacio espacio;
    private LocalDate fecha;
    private boolean activa;
}
