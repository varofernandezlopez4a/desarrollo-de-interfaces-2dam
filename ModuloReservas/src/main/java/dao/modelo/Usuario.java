package dao.modelo;

import lombok.*;

import javax.validation.constraints.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Usuario {
    private int id;
    @NotBlank
    @NotEmpty
    private String nombre;
    @NotBlank
    @NotEmpty
    private String password;
    @Email
    @NotEmpty
    @NotBlank
    private String mail;
    @Min(0)
    @Max(1)
    private int tipoUsuario;
}
