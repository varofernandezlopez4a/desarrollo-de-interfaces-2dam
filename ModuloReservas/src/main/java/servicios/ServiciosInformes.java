package servicios;

import dao.DaoInformes;

public class ServiciosInformes {
    public void generarInforme1(int id) {
        DaoInformes daoInformes = new DaoInformes();
        daoInformes.generarInforme1(id);
    }

    public void generarInforme2() {
        DaoInformes daoInformes = new DaoInformes();
        daoInformes.generarInforme2();
    }

    public void generarInforme3() {
        DaoInformes daoInformes = new DaoInformes();
        daoInformes.generarInforme3();
    }
}
