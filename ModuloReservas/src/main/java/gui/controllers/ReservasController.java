package gui.controllers;

import dao.modelo.Espacio;
import dao.modelo.Reserva;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import servicios.ServiciosEspacios;
import servicios.ServiciosInformes;
import servicios.ServiciosReservas;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class ReservasController implements Initializable {


    @FXML
    private ComboBox comboInformes;
    @FXML
    private TableView<Espacio> tableViewEspacios;
    @FXML
    private TableColumn tablecolumnNombre;
    @FXML
    private TableColumn tableColumnInicio;
    @FXML
    private TableColumn tableColumnFin;
    @FXML
    private TableColumn tableColumnPrecio;
    @FXML
    private Button botonReservar;
    @FXML
    private AnchorPane reservasPane;
    @FXML
    private DatePicker fechaReserva;
    @FXML
    private TableView<Reserva> tableViewReservas;
    @FXML
    private TableColumn tableColumnEspacio;
    @FXML
    private TableColumn tableColumnFecha;
    @FXML
    private DatePicker dateReserva;

    public TableView<Reserva> getListReservas() {
        tableViewReservas.getItems().clear();
        return tableViewReservas;
    }

    private Alert alert;
    private Alert alertBakana;
    private Alert alertConfirmar;

    private ServiciosEspacios serviciosEspacios;

    private ServiciosReservas serviciosReservas;

    private ServiciosInformes serviciosInformes;

    private PrincipalController principalController;

    public void setPrincipalController(PrincipalController principalController) {
        this.principalController = principalController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fechaReserva.setValue(null);
        alert = new Alert(Alert.AlertType.ERROR);
        alertBakana = new Alert(Alert.AlertType.INFORMATION);
        alertConfirmar = new Alert(Alert.AlertType.CONFIRMATION);
        serviciosEspacios = new ServiciosEspacios();
        serviciosReservas = new ServiciosReservas();
        serviciosInformes = new ServiciosInformes();
        tablecolumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tableColumnInicio.setCellValueFactory(new PropertyValueFactory<>("hora_inicio"));
        tableColumnFin.setCellValueFactory(new PropertyValueFactory<>("hora_final"));
        tableColumnPrecio.setCellValueFactory(new PropertyValueFactory<>("precio"));
        tableColumnEspacio.setCellValueFactory(new PropertyValueFactory<>("espacio"));
        tableColumnFecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        comboInformes.getItems().addAll("El precio de cada espacio");

    }

    public void cargarEspacios() {
        tableViewEspacios.getItems().clear();
        serviciosEspacios.getAllEspacios()
                .peek(espacios -> {
                    if (!espacios.isEmpty()) {
                        espacios.stream().forEach(espacio -> tableViewEspacios.getItems().add(espacio));
                    } else {
                        alertBakana.setContentText("No hay espacios");
                        alertBakana.showAndWait();
                    }
                })
                .peekLeft(message -> {
                    alert.setContentText(message);
                    alert.showAndWait();
                });
    }

    @FXML
    private void comprobarDia(ActionEvent mouseEvent) {
        if (fechaReserva.getValue() != null) {
            if (fechaReserva.getValue().isAfter(LocalDate.now())) {
                if (fechaReserva.getValue().getDayOfWeek().ordinal() == 6) {
                    botonReservar.setDisable(true);
                    alert.setContentText("No puedes reservar un domingo");
                    alert.showAndWait();
                    fechaReserva.setValue(null);
                } else {
                    botonReservar.setDisable(false);
                }
            } else {
                alert.setContentText("La fecha no puede ser anterior a hoy");
                alert.showAndWait();
            }
        }
    }

    public void reservar(ActionEvent actionEvent) throws InvalidKeySpecException, NoSuchAlgorithmException {
        Espacio espacio = tableViewEspacios.getSelectionModel().getSelectedItem();
        Reserva reserva = new Reserva(0,
                principalController.getUsuario().getId(),
                espacio,
                fechaReserva.getValue(),
                true);

        if (fechaReserva.getValue() != null && espacio != null) {
            alertConfirmar.setContentText("El precio de la reserva de este espacio es: \n" + espacio.getPrecio() + "€\n¿Desea continuar?");
            Optional<ButtonType> decision = alertConfirmar.showAndWait();
            if (decision.get() == ButtonType.OK) {
                serviciosReservas.hacerReserva(reserva).peek(r -> {
                            alertBakana.setContentText("Reserva confirmada\n" +
                                    "¡Muchas gracias!");
                            alertBakana.showAndWait();
                            tableViewReservas.getItems().clear();
                            principalController.cargarReservas();
                        }
                ).peekLeft(message -> {
                    alert.setContentText(message);
                    alert.showAndWait();
                });
            } else {
                alertBakana.setContentText("Operacion cancelada");
                alertBakana.showAndWait();
            }

        } else {
            alert.setContentText("Algun campo vacio");
            alert.showAndWait();
        }
    }

    public void botonCancelar(ActionEvent actionEvent) {
        Reserva reservaModificar = tableViewReservas.getSelectionModel().getSelectedItem();

        if (reservaModificar != null) {
            serviciosReservas.cancelarReserva(reservaModificar)
                    .peek(s -> {
                        alertBakana.setContentText("Reserva cancelada");
                        alertBakana.showAndWait();
                        tableViewReservas.getItems().clear();
                        principalController.cargarReservas();
                        dateReserva.setValue(null);
                    })
                    .peekLeft(message -> {
                        alert.setContentText(message);
                        alert.showAndWait();
                    });
        } else {
            alert.setContentText("Debe seleccionar la reserva");
            alert.showAndWait();
        }
    }

    public void botonModificar(ActionEvent actionEvent) {
        Reserva reservaModificar = tableViewReservas.getSelectionModel().getSelectedItem();

        if (reservaModificar != null) {
            if (dateReserva.getValue() != null) {
                if (dateReserva.getValue().isBefore(LocalDate.now())) {
                    alert.setContentText("La nueva fecha NO puede ser anterior al dia de hoy");
                    alert.showAndWait();
                } else {
                    reservaModificar.setFecha(dateReserva.getValue());
                    serviciosReservas.actualizarReserva(reservaModificar)
                            .peek(s -> {
                                alertBakana.setContentText("Reserva actualizada");
                                alertBakana.showAndWait();
                                tableViewReservas.getItems().clear();
                                principalController.cargarReservas();
                                dateReserva.setValue(null);
                            })
                            .peekLeft(message -> {
                                alert.setContentText(message);
                                alert.showAndWait();
                            });
                }
            } else {
                alert.setContentText("Elija la nueva fecha");
                alert.showAndWait();
            }
        } else {
            alert.setContentText("Debe seleccionar la reserva");
            alert.showAndWait();
        }
    }

    public void generarInforme(ActionEvent actionEvent) {

        if (comboInformes.getSelectionModel().getSelectedIndex() == 0) {
            serviciosInformes.generarInforme1(principalController.getUsuario().getId());
        } else if (comboInformes.getSelectionModel().getSelectedIndex() == 1) {
            serviciosInformes.generarInforme2();
        } else if (comboInformes.getSelectionModel().getSelectedIndex() == 2) {
            serviciosInformes.generarInforme3();
        } else {

        }


    }
}
