package gui.controllers;

import dao.modelo.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class BienvenidaController implements Initializable {
    @FXML
    private AnchorPane bienvenidaPane;
    @FXML
    private Button botonReservas;
    @FXML
    private Button botonCuenta;

    @FXML
    private Label textBienvenida;

    private PrincipalController borderPane;


    public void setBorderPane(PrincipalController borderPane) {
        this.borderPane = borderPane;
    }

    public void cargarMensajeBienvanida(Usuario usuario) {
        textBienvenida.setText("BIENVENID@ " + usuario.getNombre().toUpperCase());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Tooltip tooltipCuenta=new Tooltip();
        Tooltip tooltipReservas=new Tooltip();
        tooltipCuenta.setText("Haz clic para ver tu cuenta");
        tooltipReservas.setText("Haz clic para acceder a reservas");

        botonCuenta.setTooltip(tooltipCuenta);
        botonReservas.setTooltip(tooltipReservas);
    }


    public void cargarReservas(ActionEvent actionEvent) {
        borderPane.cargarReservas();
    }

    public void cargarCuenta(ActionEvent actionEvent) {
        borderPane.menuBotonCuenta();
    }
}
