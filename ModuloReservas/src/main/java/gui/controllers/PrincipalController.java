package gui.controllers;

import dao.modelo.Reserva;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import servicios.ServiciosReservas;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PrincipalController implements Initializable {

    Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @FXML
    private AnchorPane bar;
    @FXML
    private BorderPane mainPane;
    Alert alert;

    private AnchorPane loginPane;
    private LoginController loginController;

    private AnchorPane bienvenidaPane;
    private BienvenidaController bienvenidaController;

    private TabPane reservasPane;
    private ReservasController reservasController;


    private AnchorPane gestionarCuentaPane;
    private GestionarCuentaController gestionarCuentaController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.ERROR);
        cargarLogin();
    }

    @FXML
    private void cargarLogin() {
        try {
            bar.setVisible(false);
            if (loginPane == null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/login.fxml"));
                loginPane = loader.load();
                loginController = loader.getController();
                loginController.setBorderPane(this);
                mainPane.setCenter(loginPane);
            }
            mainPane.setCenter(loginPane);
        } catch (Exception e) {
            alert.setContentText("Fallo");
            alert.show();
        }
    }

    @FXML
    protected void menuBotonCuenta() {
        try {
            if (gestionarCuentaPane == null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gestionarCuenta.fxml"));
                gestionarCuentaPane = loader.load();
                gestionarCuentaController = loader.getController();
                gestionarCuentaController.setPrincipalController(this);
            }
            gestionarCuentaController.getTextNombre().setText(usuario.getNombre());
            gestionarCuentaController.getTextMail().setText(usuario.getMail());
            mainPane.setCenter(gestionarCuentaPane);
        } catch (Exception e) {
            alert.setContentText("Fallo");
            alert.show();
        }
    }

    @FXML
    protected void cargarBienvenida() {
        try {
            bar.setVisible(true);
            if (bienvenidaPane == null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/bienvenida.fxml"));
                bienvenidaPane = loader.load();
                bienvenidaController = loader.getController();
                bienvenidaController.setBorderPane(this);
                mainPane.setCenter(bienvenidaPane);
            }
            bienvenidaController.cargarMensajeBienvanida(usuario);
            mainPane.setCenter(bienvenidaPane);
        } catch (Exception e) {
            alert.setContentText("Fallo");
        }
    }

    public void logout(ActionEvent actionEvent) {

        cargarLogin();
        setUsuario(null);
    }


    public void cargarReservas() {
        try {
            if (reservasPane == null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/reservasPane.fxml"));
                reservasPane = loader.load();
                reservasController = loader.getController();
                reservasController.setPrincipalController(this);
            }
            reservasController.cargarEspacios();
            ServiciosReservas serviciosReservas = new ServiciosReservas();
            Either<String, List<Reserva>> selectReservas = serviciosReservas.getTodasReservasUsuario(usuario);
            selectReservas.peek(reservas -> {
                reservasController.getListReservas().getItems().addAll(reservas);
            });
            mainPane.setCenter(reservasPane);
        } catch (Exception e) {
            alert.setContentText("Fallo");
            alert.show();
        }
    }

}
