package dao;

import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

@Log4j2
public class DaoUsuarios {
    private String QUERY_INSERT_USER = "insert into Usuarios  (nombre,password,mail, tipo_usuario) " +
            "values (?,?,?, ?)";

    private static final String SELECT_ONE_USER_FOR_LOGIN =
            "select * from Usuarios u " +
                    "where u.nombre = ?";

    private static String ACTUALIZAR_USUARIO =
            "update Usuarios u set u.password = ?, u.mail = ?, u.nombre = ? " +
                    "where u.id = ?";

    public Either<String, Boolean> addUser(Usuario usuario) {
        Either<String, Boolean> add = null;
        DBConnection db = new DBConnection();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = db.getConnection();
            stmt = con.prepareStatement
                    (QUERY_INSERT_USER,
                            Statement.RETURN_GENERATED_KEYS);

            stmt.setString(1, usuario.getNombre());
            stmt.setString(2, usuario.getPassword());
            stmt.setString(3, usuario.getMail());
            stmt.setInt(4, usuario.getTipoUsuario());

            int numeroFilas = stmt.executeUpdate();

            if (numeroFilas == 1) {
                add = Either.right(true);
            }


        } catch (Exception e) {
            log.error(e.getMessage());
            add = Either.left("No se pudo añadir");
        } finally {
            db.cerrarResultSet(rs);
            db.cerrarStatement(stmt);
            db.cerrarConexion(con);
        }


        return add;
    }

    public Either<String, Usuario> login(String usuario) {
        DBConnection dbConnection = new DBConnection();
        Connection connection = null;
        Usuario user = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Either<String, Usuario> result = null;

        try {
            connection = dbConnection.getConnection();
            preparedStatement = connection.prepareStatement(SELECT_ONE_USER_FOR_LOGIN);
            preparedStatement.setString(1, usuario);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new Usuario(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("password"),
                        resultSet.getString("mail"),
                        resultSet.getInt("tipo_usuario")
                        );
                result = Either.right(user);
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            result = Either.left("Datos erroneos");
        } finally {
            dbConnection.cerrarConexion(connection);
            dbConnection.cerrarResultSet(resultSet);
            dbConnection.cerrarStatement(preparedStatement);
        }

        return result;
    }

    public Either<String, Boolean> updateUser(Usuario usuario) {
        Either<String, Boolean> actualizado = null;
        DBConnection db = new DBConnection();
        Connection con = null;
        PreparedStatement stmt = null;
        int resultSetInt = 0;
        try {
            con = db.getConnection();
            stmt = con.prepareStatement(ACTUALIZAR_USUARIO);
            stmt.setString(1, usuario.getPassword());
            stmt.setString(2, usuario.getMail());
            stmt.setString(3, usuario.getNombre());
            stmt.setLong(4, usuario.getId());
            resultSetInt = stmt.executeUpdate();
        } catch (Exception e) {
            log.error(e.getMessage());
            actualizado = Either.left("No se pudo actualizar");
        } finally {
            db.cerrarStatement(stmt);
            db.cerrarConexion(con);
        }
            if(resultSetInt > 0){
                actualizado = Either.right(true);
            }else{
                actualizado = Either.left("No se pudo actualizar");
            }
        return actualizado;
    }

}
