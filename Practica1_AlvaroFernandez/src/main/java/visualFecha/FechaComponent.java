package visualFecha;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.GregorianCalendar;

import static java.time.temporal.ChronoUnit.DAYS;

public class FechaComponent extends JPanel {
    private JPanel panelFecha;
    private JTextField textDia;
    private JTextField textMes;
    private JTextField textAño;
    private JButton ComprobarButton;

    public FechaComponent() {
        this.add(panelFecha);
    }

    public Container comprobarFecha() {
        ComprobarButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String mes = textMes.getText();
                String dia = textDia.getText();
                String año = textAño.getText();
                if (mes.isEmpty() || !mes.chars().allMatch(Character::isDigit)) {
                    JOptionPane.showMessageDialog(null, "Introduzca un mes (numerico)");
                } else if (dia.isEmpty() || !dia.chars().allMatch(Character::isDigit)) {
                    JOptionPane.showMessageDialog(null, "Introduzca un dia (numerico)");
                } else if (año.isEmpty() || (Integer.parseInt(año) < 0) || !año.chars().allMatch(Character::isDigit)) {
                    JOptionPane.showMessageDialog(null, "Introduzca un año valido");
                } else {
                    String mensaje = null;
                    try {
                        mensaje = comprobarFecha(Integer.parseInt(mes), Integer.parseInt(dia), Integer.parseInt(año));
                        JOptionPane.showMessageDialog(null, mensaje);
                    } catch (Exception x) {
                        JOptionPane.showMessageDialog(null, x.getMessage());
                    }
                }
            }
        });
        return null;
    }

    private String comprobarFecha(int mes, int dia, int año) {
        String mensaje = null;
        try {
            switch (mes) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    if (dia > 31) {
                        mensaje = "El dia debe ser inferior a 31";

                    } else {
                        long diasPasados = numeroDiasEntreDosFechas(dia, mes, año);
                        mensaje = "Datos correctos y han pasado " + diasPasados + " dias hasta hoy";
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    if (dia > 30) {
                        mensaje = "El dia debe ser inferior a 30";
                    } else {
                        long diasPasados = numeroDiasEntreDosFechas(dia, mes, año);
                        mensaje = "Datos correctos y han pasado " + diasPasados + " dias hasta hoy";
                    }
                    break;
                case 2:
                    GregorianCalendar gregorianCalendar = new GregorianCalendar();
                    if (gregorianCalendar.isLeapYear(año)) {
                        if (dia > 29) {
                            mensaje = "El dia debe ser inferior a 29";
                        } else {
                            long diasPasados = numeroDiasEntreDosFechas(dia, mes, año);
                            mensaje = "Datos correctos y han pasado " + diasPasados + " dias hasta hoy";
                        }
                    } else if (dia > 28) {
                        mensaje = "El dia debe ser inferior a 28 ya que el año seleccionado no es bisiesto";
                    } else {
                        long diasPasados = numeroDiasEntreDosFechas(dia, mes, año);
                        mensaje = "Datos correctos y han pasado " + diasPasados + " dias hasta hoy";
                    }
                    break;
                default:
                    mensaje = "Introduzca un mes entre 1 y 12";
                    break;
            }
        } catch (Exception e) {
            mensaje = "El patron de la fecha debe ser (DD/MM/AAAA)";
        }
        textAño.setText("");
        textDia.setText("");
        textMes.setText("");

        return mensaje;
    }


    public long numeroDiasEntreDosFechas(int dia, int mes, int año) {
        String requestDate = "";
        if (dia < 10 && mes < 10) {
            requestDate = año + "-0" + mes + "-0" + dia;
        } else if (dia < 10) {
            requestDate = año + "-" + mes + "-0" + dia;
        } else if (mes < 10) {
            requestDate = año + "-0" + mes + "-" + dia;
        } else {
            requestDate = año + "-" + mes + "-" + dia;
        }

        LocalDate myDate = LocalDate.parse(requestDate);

        LocalDate currentDate = LocalDate.now();

        long numberOFDays = DAYS.between(myDate, currentDate);

        return numberOFDays;
    }

}

