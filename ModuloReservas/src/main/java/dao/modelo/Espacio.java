package dao.modelo;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Espacio {

    private int id;
    private String nombre;
    private double precio;
    private Timestamp hora_inicio;
    private Timestamp hora_final;


    @Override
    public String toString() {
        return nombre;
    }
}
