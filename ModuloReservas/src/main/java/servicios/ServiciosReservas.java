package servicios;

import dao.DaoReservas;
import dao.modelo.Reserva;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public class ServiciosReservas {
    public Either<String, Boolean> hacerReserva(Reserva reserva) throws InvalidKeySpecException, NoSuchAlgorithmException {
        DaoReservas daoReservas = new DaoReservas();
        return daoReservas.hacerReserva(reserva);
    }

    public Either<String, List<Reserva>> getTodasReservasUsuario(Usuario u) {
        DaoReservas daoReservas = new DaoReservas();
        return daoReservas.getTodasReservasUsuario(u);
    }

    public Either<String, Boolean> actualizarReserva(Reserva reserva) {
        DaoReservas daoReservas = new DaoReservas();
        return daoReservas.actualizarReserva(reserva);
    }

    public Either<String, Boolean> cancelarReserva(Reserva reservaModificar) {
        DaoReservas daoReservas = new DaoReservas();
        return daoReservas.cancelarReserva(reservaModificar);
    }
}
