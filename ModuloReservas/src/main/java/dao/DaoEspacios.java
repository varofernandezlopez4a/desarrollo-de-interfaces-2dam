package dao;

import dao.modelo.Espacio;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DaoEspacios {

    private String SELECT_ALL_ESPACIOS = "select * from Espacio ";

    public Either<String, List<Espacio>> getAllEspacios() {
        List<Espacio> espacios = new ArrayList<>();
        Either<String, List<Espacio>> result = null;
        DBConnection dbConnection = new DBConnection();
        Connection connection = null;
        Espacio espacio = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {

            connection = dbConnection.getConnection();


            preparedStatement = connection.prepareStatement(
                    SELECT_ALL_ESPACIOS);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                espacio = new Espacio(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getDouble("precio"),
                        resultSet.getTimestamp("hora_inicio"),
                        resultSet.getTimestamp("hora_fin"));
                espacios.add(espacio);
            }
            result = Either.right(espacios);
        } catch (Exception e) {
            log.error(e.getMessage());
            result = Either.left(e.getMessage());
        } finally {
            dbConnection.cerrarResultSet(resultSet);
            dbConnection.cerrarStatement(preparedStatement);
            dbConnection.cerrarConexion(connection);
        }
        return result;
    }
}
