package servicios;

import dao.DaoUsuarios;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.atomic.AtomicReference;

public class ServiciosUsuarios {
    public Either<String, Boolean> registrarse(Usuario usuario) throws InvalidKeySpecException, NoSuchAlgorithmException {
        DaoUsuarios daoUsuarios = new DaoUsuarios();
        String newPass = utils.PasswordHash.createHash(usuario.getPassword());
        usuario.setPassword(newPass);
        return daoUsuarios.addUser(usuario);
    }

    public Either<String, Usuario> login(String usuario, String pass) throws InvalidKeySpecException, NoSuchAlgorithmException {
        AtomicReference<Either<String, Usuario>> result = new AtomicReference<>();
        DaoUsuarios daoUsuarios = new DaoUsuarios();
        daoUsuarios.login(usuario).peek(user -> {
            try {
                if (utils.PasswordHash.validatePassword(pass, user.getPassword())) {
                    result.set(Either.right(user));
                } else {
                    result.set(Either.left("Datos erroneos"));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        });

        return result.get();
    }

    public Either<String, Boolean> updateUser(Usuario usuario) {
        DaoUsuarios daoUsuarios = new DaoUsuarios();
        return daoUsuarios.updateUser(usuario);
    }
}
