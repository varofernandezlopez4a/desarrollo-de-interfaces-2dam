package servicios;

import dao.DaoEspacios;
import dao.modelo.Espacio;
import io.vavr.control.Either;

import java.util.List;

public class ServiciosEspacios {
    public Either<String, List<Espacio>> getAllEspacios() {
        DaoEspacios daoEspacios=new DaoEspacios();
        return daoEspacios.getAllEspacios();
    }
}
