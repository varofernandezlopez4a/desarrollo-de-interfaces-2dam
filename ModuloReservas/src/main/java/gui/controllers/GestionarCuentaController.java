package gui.controllers;

import dao.modelo.Usuario;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import servicios.ServiciosUsuarios;
import utils.PasswordHash;

import java.net.URL;
import java.util.ResourceBundle;

public class GestionarCuentaController implements Initializable {
    @FXML
    private TextField textNombre;
    @FXML
    private TextField textMail;
    @FXML
    private PasswordField textPassword;

    public TextField getTextNombre() {
        return textNombre;
    }

    public TextField getTextMail() {
        return textMail;
    }

    public PasswordField getTextPassword() {
        return textPassword;
    }

    private Alert alertInfo;
    private Alert alertError;

    private PrincipalController principalController;

    public void setPrincipalController(PrincipalController principalController) {
        this.principalController = principalController;
    }

    public void botonActualizar(ActionEvent actionEvent) {
        if (!textNombre.getText().isEmpty() && !textMail.getText().isEmpty() && !textPassword.getText().isEmpty()) {
            PasswordHash passwordHash = new PasswordHash();
            ServiciosUsuarios serviciosUsuarios = new ServiciosUsuarios();
            try {
                String pass = passwordHash.createHash(textPassword.getText());
                Usuario u = new Usuario(
                        principalController.getUsuario().getId(),
                        textNombre.getText(),
                        pass,
                        textMail.getText(),
                        principalController.getUsuario().getTipoUsuario()
                );
                Either<String, Boolean> updateUsuario = serviciosUsuarios.updateUser(u);
                updateUsuario.peek(o -> {
                    alertInfo.setContentText("Actualizado");
                    alertInfo.show();
                    principalController.setUsuario(u);
                }).peekLeft(s -> {
                    alertError.setContentText("ERROR AL ACTUALIZAR");
                    alertError.show();
                });
            } catch (Exception e) {
                alertError.setContentText("ERROR");
                alertError.show();
            }

        } else {
            alertError.setContentText("CAMPOS VACIOS");
            alertError.show();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        alertInfo = new Alert(Alert.AlertType.INFORMATION);
        alertError = new Alert(Alert.AlertType.ERROR);
    }
}
