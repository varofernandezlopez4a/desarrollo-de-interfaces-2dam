package visualFecha;

import javax.swing.*;

public class Main {

    private JPanel main;


    public static void main(String[] args) {
        JFrame frame = new JFrame("MainSwing");
        frame.setContentPane(new Main().main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        FechaComponent fechaComponent = new FechaComponent();
        frame.setContentPane(fechaComponent);
        fechaComponent.comprobarFecha();
        frame.setBounds(0,0,600,300);

    }
}
